import Vue from 'vue'
import Router from 'vue-router'
import taskList from '@/components/taskList'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/all',
      name: 'tasks.all',
      component: taskList,
      props: {
        filter: 'all'
      }
    },
    {
      path: '/open',
      name: 'tasks.open',
      component: taskList,
      props: {
        filter: 'open'
      }
    },
    {
      path: '/done',
      name: 'tasks.done',
      component: taskList,
      props: {
        filter: 'done'
      }
    },
    {
      path: '*',
      redirect: '/all'
    }
  ]
})
