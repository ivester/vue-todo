import Vue from 'vue'
import Vuex from 'vuex'
import tasks from './modules/tasks'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    tasks
  },
  mutations: {
    initializeStore (state) {
      const store = localStorage.getItem('store')
      if (store) {
        this.replaceState(Object.assign({}, state, JSON.parse(store)))
      }
    }
  },
  strict: debug
})
