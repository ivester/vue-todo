import uuid from 'uuid/v1'
import { findIndex } from 'lodash'

const taskIndex = (tasks, task) => findIndex(tasks, o => o.key === task.key)

export const types = {
  ADD_TASK: 'tasks/addTask',
  UPDATE_TASK: 'tasks/updateTask',
  REMOVE_TASK: 'tasks/removeTask',
  FILTER_TASKS: 'tasks/filterTasks',
  TOGGLE_TASK_DONE: 'tasks/toggleTaskDone'
}

export const defaultTask = () => {
  return {
    key: uuid(),
    done: false,
    text: ''
  }
}

const state = {
  tasks: []
}

const getters = {
  [types.FILTER_TASKS]: ({ tasks }) => filter => {
    if (filter === 'done') {
      return tasks.filter(o => o.done)
    }
    if (filter === 'open') {
      return tasks.filter(o => !o.done)
    }
    return tasks
  }
}

const mutations = {
  [types.ADD_TASK] ({ tasks }, task) {
    tasks.unshift(task)
  },
  [types.UPDATE_TASK] ({ tasks }, task) {
    const i = taskIndex(tasks, task)
    if (i < 0) { return }
    tasks.splice(i, 1, task)
  },
  [types.REMOVE_TASK] ({ tasks }, task) {
    const i = taskIndex(tasks, task)
    if (i < 0) { return }
    tasks.splice(i, 1)
  }
}

const actions = {
  [types.TOGGLE_TASK_DONE] ({ commit, state }, task) {
    commit(types.UPDATE_TASK, {
      ...task,
      done: !task.done
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
